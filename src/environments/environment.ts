// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  authApiUrl: 'http://localhost:4000',
  firebaseConfig: {
    apiKey: "AIzaSyBf9rERMLw8vEr4zd1jf3Jao1KGW5EYTdA",
    authDomain: "asequizz.firebaseapp.com",
    databaseURL: "https://asequizz.firebaseio.com",
    projectId: "asequizz",
    storageBucket: "asequizz.appspot.com",
    messagingSenderId: "315846552941"
  },
  
  // backendHost: 'http://localhost:3000',
  backendHost: 'http://frizeriamica.ro:3000',
  // backendHost: ' https://us-central1-asequizz.cloudfunctions.net/proxyData/?rest=',
  backendPort: 3000,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
