import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DataService } from '../shared/data.service';

import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quiz-pool',
  templateUrl: './quiz-pool.component.html',
  styleUrls: ['./quiz-pool.component.css']
})
export class QuizPoolComponent implements OnInit {

  backendHost = environment.backendHost;
  quizes;

  constructor(
    private http: HttpClient,
    private data: DataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getQuizes();
  }

  getQuizes() {
    const quizRef = this.http.get(`${this.backendHost}/quizes`).subscribe((quizes) => {
      this.quizes = quizes;
      console.log(quizes);
      quizRef.unsubscribe();
    });
  }

  reviewQuiz(id) {
    this.navigate('quiz-take/' + id);
  }

  toggleActive(id) {
    console.log(id);
    this.http.get(`${this.backendHost}/quiz/activate/` + id)
    .subscribe().unsubscribe();
  }

  editQuiz(id) {
    const index = this.quizes.findIndex((quiz) => {
      return quiz.id == id;
    });
    this.data.changeActiveEditorQuiz(
      JSON.stringify(this.quizes[index].structure)
    );
    this.navigate('editor');
  }

  deleteQuiz(id) {
    const index = this.quizes.findIndex((quiz) => {
      return quiz.id == id;
    });
    this.quizes.splice(index, 1);

    this.http.get(`${this.backendHost}/quiz/delete/` + id)
    .subscribe().unsubscribe();
    console.log('Quiz Deleted');
  }


  navigate(path) {
    this.router.navigate([path]);
  }
}
