import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { environment } from './../../environments/environment'

@Component({
  selector: 'app-pending-quizes',
  templateUrl: './pending-quizes.component.html',
  styleUrls: ['./pending-quizes.component.css']
})
export class PendingQuizesComponent implements OnInit {

  backendHost = environment.backendHost;
  quizes;

  constructor(
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getQuizes();
  }

  getQuizes() {
    const quizRef = this.http.get(`${this.backendHost}/quizes`).subscribe((quizes) => {
      this.quizes = quizes;
      var index = 0;
      this.quizes.map((quiz) => {
        if (!quiz.active) {
          this.quizes.splice(index, 1);
        }
        index++;
      });
      
      quizRef.unsubscribe();
    });
  }

  reviewQuiz(id) {
    this.navigate('quiz-take/' + id);
  }

  navigate(path) {
    this.router.navigate([path]);
  }
}
