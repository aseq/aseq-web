import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '../_models/user';
import { UserService } from '../core/user.service';
import { AuthService } from '../core/auth.service';

import * as SurveyEditor from 'surveyjs-editor';
import * as Survey from 'survey-angular';

@Component({templateUrl: 'home.component.html'})
export class HomeComponent implements OnInit {
    users: User[] = [];
    editor: SurveyEditor.SurveyEditor;

    constructor(
      private userService: UserService,
      private auth: AuthService
    ) {}

    ngOnInit() {
        Survey.StylesManager.applyTheme("bootstrap");
        this.editor = new SurveyEditor.SurveyEditor('editorElement', {});

        this.userService.getAll().pipe(first()).subscribe(users => { 
            this.users = users; 
        });

        var json = { questions: [
            { type: "radiogroup", name: "car", title: "What car are you driving?", isRequired: true, 
            colCount: 4, choices: ["None", "Ford", "Vauxhall", "Volkswagen", "Nissan", "Audi", "Mercedes-Benz", "BMW", "Peugeot", "Toyota", "Citroen"] }
        ]};

        
        var survey = new Survey.Model(json);
        survey.onComplete.add((asd) => {
            console.log(survey.data);
        });
        
        Survey.SurveyNG.render('surveyContainer', { model: survey });
        // console.log(model);
        // console.log(this.editor);
    }
}