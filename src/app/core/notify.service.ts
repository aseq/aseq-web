import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { TempDialogComponent } from './notify-comps/temp-dialog/temp-dialog.component';

import { DataService } from '../shared/data.service';

/// Notify users about errors and other helpful stuff
export interface Msg {
  content: string;
  style: string;
}

@Injectable()
export class NotifyService {

  constructor(
    public dialog: MatDialog,
    private dataService: DataService
  ) { }
  
  openDialog() {
    const dialogRef = this.dialog.open(TempDialogComponent, {});

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  update(message: string, style?: any) {
    this.dataService.changeMessage(message);
    this.openDialog();
  }





}

