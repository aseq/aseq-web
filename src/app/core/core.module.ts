import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthService } from './auth.service';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { UserService } from './user.service';
import { NotifyService } from './notify.service';
import { TempDialogComponent } from './notify-comps/temp-dialog/temp-dialog.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TempDialogComponent],
  providers: [AuthService, UserService, NotifyService]
})
export class CoreModule { }
