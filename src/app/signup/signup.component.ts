import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth.service';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {matchOtherValidator} from '../core/matchOtherValidator';

interface SignUpUser {
  email: string;
  password: string;
  passwordConfirm: string;
  displayName: string;
  type: string;
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup;
  loginForm: FormGroup;
  signUpUser: SignUpUser = {email: '', password: '', passwordConfirm: '', displayName: '', type: 'member'};


  constructor(public auth: AuthService, private fb: FormBuilder) { }

  ngOnInit() {
    this.signUpForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [
        Validators.required,
        Validators.pattern('^(?=.*[0-9])|(?=.*[!@#\$%\^&\*])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6)
      ]],
      displayName: ['', [Validators.required]]});
    this.signUpForm.valueChanges.subscribe((data: any) => { this.signUpUser = data; this.signUpUser.type = 'member'; });
  }

  //
  // ─── FormBuilderStuff  ───────────────────────────────────────────────────────────────────────────
  //

  passwordMatch() {
    console.log(this.signUpUser.password === this.signUpUser.passwordConfirm);
    return this.signUpUser.password === this.signUpUser.passwordConfirm;
  }

  get signUpEmail() {
    return this.signUpForm.get('email');
  }

  get signUpPassword() {
    return this.signUpForm.get('password');
  }

  get signUpDisplayName() {
    return this.signUpForm.get('displayName');
  }

  get signUpPasswordConfirm() {
    return this.signUpForm.get('passwordConfirm');
  }

}
