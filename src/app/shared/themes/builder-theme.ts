import * as SurveyEditor from 'surveyjs-editor';

var defaultThemeColorsEditor =  SurveyEditor.StylesManager.ThemeColors['default'];
// defaultThemeColorsEditor["$primary-color"] = 'green';
// defaultThemeColorsEditor["$secondary-color"] = 'orange';
// defaultThemeColorsEditor["$primary-hover-color"] = 'purple';
// defaultThemeColorsEditor["$primary-text-color"] = 'black';
// defaultThemeColorsEditor["$selection-border-color"] = 'brown';
// defaultThemeColorsEditor["$background-color"] = 'brown';

export const builderTheme = defaultThemeColorsEditor;
