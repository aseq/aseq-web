export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyBf9rERMLw8vEr4zd1jf3Jao1KGW5EYTdA",
    authDomain: "asequizz.firebaseapp.com",
    databaseURL: "https://asequizz.firebaseio.com",
    projectId: "asequizz",
    storageBucket: "asequizz.appspot.com",
    messagingSenderId: "315846552941"
  },
  
  backendHost: 'http://frizeriamica.ro:3000',
  // backendHost: ' https://us-central1-asequizz.cloudfunctions.net/proxyData/?rest=',
  backendPort: 3000
};
