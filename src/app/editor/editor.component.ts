import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../core/auth.service';

import * as SurveyEditor from 'surveyjs-editor';
import * as Survey from 'survey-angular';
import { builderTheme } from '../shared/themes/builder-theme';

import { environment } from '../../environments/environment';
import { DataService } from '../shared/data.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {

  editor;
  survey;
  maxTime = null;
  random = false;
  backendHost = environment.backendHost;

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private data: DataService
  ) { }

  ngOnInit() {
    builderTheme;

    this.editor = new SurveyEditor.SurveyEditor('editorElement', {});
    
    this.editor
    .toolbox
    .changeCategories([
        {
            name: "panel",
            category: "Panels"
        }, {
            name: "paneldynamic",
            category: "Panels"
        }, {
            name: "matrix",
            category: "Matrix"
        }, {
            name: "matrixdropdown",
            category: "Matrix"
        }, {
            name: "matrixdynamic",
            category: "Matrix"
        }
    ]);
    
    this.editor.saveSurveyFunc = () => {
      this.survey = this.editor.text;
      const data = {
        quiz_name: this.survey.title || 'Default Title',
        structure: this.survey,
      };
      console.log(data);
      
      this.http.get(`${this.backendHost}/students`).subscribe((data) => {
        console.log(data);
      });

      this.http.post(`${this.backendHost}/quiz/new`, data).subscribe((d) => {
        console.log(d);
      })
    }
    
    document.getElementsByClassName('svd_commercial_container')[0].remove();

    this.data.editorData.subscribe((editorQuiz) => {
      this.editor.text = editorQuiz;
    });
  }

}
