import * as functions from 'firebase-functions';
import { proxyData } from './proxyData';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript

exports.proxyData = proxyData;
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

