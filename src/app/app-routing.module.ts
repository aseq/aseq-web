import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './core/auth.guard';
import { SignupComponent } from './signup/signup.component';
import { EditorComponent } from './editor/editor.component';
import { QuizPoolComponent } from './quiz-pool/quiz-pool.component';
import { QuizTakeComponent } from './quiz-take/quiz-take.component'
import { PendingQuizesComponent } from './pending-quizes/pending-quizes.component';
import { StudentPoolComponent } from './student-pool/student-pool.component';
 
const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'editor', component: EditorComponent },
  { path: 'quiz-pool', component: QuizPoolComponent },
  { path: 'quiz-take/:id', component: QuizTakeComponent },
  { path: 'pending-quizes', component: PendingQuizesComponent },
  { path: 'student-pool', component: StudentPoolComponent },

  { path: '*', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
