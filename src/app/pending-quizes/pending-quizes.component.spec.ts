import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingQuizesComponent } from './pending-quizes.component';

describe('PendingQuizesComponent', () => {
  let component: PendingQuizesComponent;
  let fixture: ComponentFixture<PendingQuizesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingQuizesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingQuizesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
