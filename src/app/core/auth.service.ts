import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { NotifyService } from './notify.service';

import { User } from './User';



@Injectable()
export class AuthService {
  user: Observable<User>;
  userSnapshot;

  private handleError(error) {
    this.notify.update(error.message, 'error');
  }

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    public notify: NotifyService,
    private storage: AngularFireStorage
  ) {
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  checkUserStatus() {
    return firebase.auth().currentUser;
  }
  setUserSnapshot() {
    this.user.subscribe((user) => this.userSnapshot = user);
  }


//
// ─── Twitter ───────────────────────────────────────────────────────────────────────────
//

  loginWithTwitter() {
    const provider = new firebase.auth.TwitterAuthProvider();
    return firebase.auth().signInWithPopup(provider).then((result) => {
      this.setUsernameData(result);
      this.setTwitterUserData(result);
    });
  }

  setTwitterUserData(result) {
    const userInfo = result.additionalUserInfo;
    const user = result.user;

    const userRef: AngularFirestoreDocument<any> =
      this.afs.doc(`users/${user.uid}`);

    const data: User = {
      uid: user.uid,
      username: userInfo.username,
      twitterVerified: userInfo.profile.verified,
      accountType: 'twitter',
      type: 'model'
    };

    // Daca nu are photoURL deja pus pun defaultul
    userRef.ref.get().then((doc) => {
      if (!doc.data()) {
        data.coverURL = '../assets/cover.jpg';
        data.photoURL = '../assets/profile.png';
        data.displayName = user.displayName;
        data.email = user.email;
        data.completedData = false;
      }
    }).then(() => {
      return userRef.set(data, { merge: true });
    });
  }

  setUsernameData(result) {
    const userInfo = result.additionalUserInfo;
    const user = result.user;

    const usernameRef: AngularFirestoreDocument<any> =
      this.afs.doc(`usernames/${userInfo.username}`);

    const data: any = {
      userId: user.uid
    };

    return usernameRef.set(data, { merge: true });
  }

//
// ─────── Good Google&Email ───────────────────────────────────────────────────────────────────────
//

  loginWithEmail(userEmail, userPassword) {
    return this.afAuth.auth.signInWithEmailAndPassword(userEmail, userPassword);
  }

  signUpWithEmail(user) {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
    .then( credential => this.setUserDataEmail(credential.user, user))
    .catch(err => this.handleError(err));
  }

  googleLogin() {
    const provider =  new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider).then((credential) => this.setUserDataEmail(credential.user, null));
  }
  // Sets user data to firestore on EmailSignup (Used for GoogleSignIn/Up as well)
  private setUserDataEmail(user, userData) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);

    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: userData.displayName,
      type: userData.type
    };

    return userRef.set(data, { merge: true });
  }

//
// ─── Aux ───────────────────────────────────────────────────────────────────────────
//

  restorePassword(email) {
    firebase.auth().sendPasswordResetEmail(email).then(() => {
      this.notify.update('Email was sent');
    }).catch((error) => {
      this.notify.update('Please enter a valid email');
    });
  }

  deleteFirebaseUser() {
    firebase.auth().currentUser.delete().then(() => {
      // this.deleteUserData(userCopy);
      this.notify.update('UserAccount and Data successfully deleted!', 'somt');
    }, error => {
        console.log(error);
    });
  }

  deleteUser() {
    this.user.subscribe((user) => {
      if (user.accountType == 'twitter') {
        console.log('twitter');
        this.loginWithTwitter().then(() => {
          this.deleteUserData(); 
          this.deleteFirebaseUser();
        });
      } else {
          const userProvidedPassword = prompt('Please re-authenticate before moving forward');
          const credential = firebase.auth.EmailAuthProvider.credential(firebase.auth().currentUser.email, userProvidedPassword);
          firebase.auth().currentUser.reauthenticateAndRetrieveDataWithCredential(credential).then(function() {
            this.deleteUserData();
            this.deleteFirebaseUser();
          }).catch(function(err) {
            console.log(err);
          });
      }
    });
  }

  private deleteUserData() {
    const userSub = this.user.subscribe((user) => {
      this.afs.collection('users').doc(user.uid).delete();
      this.afs.collection('usernames').doc(user.username).delete();
      this.storage.storage.refFromURL(user.photoURL).delete();
      this.storage.storage.refFromURL(user.coverURL).delete();
      userSub.unsubscribe();
    });
    // const path = `users/${user.uid}`;
    // const fileRef = this.storage.ref(path);
    // console.log(fileRef.delete());
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
        this.router.navigate(['/']);
    });
  }

}






























// import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
// import { map } from 'rxjs/operators';

// import { environment } from '../../environments/environment';

// @Injectable({ providedIn: 'root' })
// export class AuthService {
//     constructor(private http: HttpClient) { }

//     login(username: string, password: string) {
//         return this.http.post<any>(`${environment.authApiUrl}/users/authenticate`, { username, password })
//             .pipe(map(user => {
//                 // login successful if there's a jwt token in the response
//                 if (user && user.token) {
//                     // store user details and jwt token in local storage to keep user logged in between page refreshes
//                     localStorage.setItem('currentUser', JSON.stringify(user));
//                 }

//                 return user;
//             }));
//     }

//     logout() {
//         // remove user from local storage to log user out
//         localStorage.removeItem('currentUser');
//     }
// }