import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth.service';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

interface SignUpUser {
  email: string;
  password: string;
  passwordConfirm: string;
  displayName: string;
  photoURL?: string;
  favoriteColor?: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signUpForm: FormGroup;
  loginForm: FormGroup;
  signUpUser: SignUpUser = {email: '', password: '', passwordConfirm: '', displayName: ''};

  email;
  password;
  
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    public auth: AuthService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {

    this.loginForm = this.fb.group({
      loginEmail: ['', [Validators.required, /*Validators.email*/]],
      loginPassword: ['', [Validators.required]],
      color: 'white'
    });

    this.loginForm.valueChanges.subscribe((data) => {
      this.email = data.loginEmail;
      this.password = data.loginPassword;
    });

  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }

    this.auth.loginWithEmail(this.email, this.password).then((res) => {
      this.navigate('');
      console.log(res);
    });
  }

  test() {
    this.auth.user.subscribe((data) => {
      console.log(data);
    });
  }

  navigate(path) {
    this.router.navigate([path]);
  }




    // // convenience getter for easy access to form fields
    // get f() { return this.loginForm.controls; }

    // onSubmit() {
    //     this.submitted = true;
    //     console.log('subss');
    //     // stop here if form is invalid
    //     if (this.loginForm.invalid) {
    //         return;
    //     }

    //     this.loading = true;
    //     this.auth.login(this.f.loginEmail.value, this.f.loginPassword.value)
    //         .pipe(first())
    //         .subscribe(
    //             data => {
    //                 this.router.navigate(['']);
    //             },
    //             error => {
    //                 this.error = error;
    //                 this.loading = false;
    //             });
    // }


  //
  // ─── FormBuildStuff ───────────────────────────────────────────────────────────────────────────
  //

  passwordMatch() {
    console.log(this.signUpUser.password === this.signUpUser.passwordConfirm);
    return this.signUpUser.password === this.signUpUser.passwordConfirm;
  }

  get signUpEmail() {
    return this.signUpForm.get('email');
  }

  get signUpPassword() {
    return this.signUpForm.get('password');
  }

  get signUpDisplayName() {
    return this.signUpForm.get('displayName');
  }

  get signUpPasswordConfirm() {
    return this.signUpForm.get('passwordConfirm');
  }
  get loginEmail() {
    return this.loginForm.get('loginEmail');
  }
  get loginPassword() {
    return this.loginForm.get('loginPassword');
  }


}
