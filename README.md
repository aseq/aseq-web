# AseQ

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.4.

## Production Server

https://asequizz.firebaseapp.com

## Backend and DB

http://frizeriamica.ro:3000


NodeJS, Express, TypeScript, Sequelize, MySql

## Environment & Macro Dependencies

######nodejs 8+ 
>For windows
&nbsp; download and install from https://nodejs.org/en/
```
# Using Ubuntu
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

# Using Arch 
sudo pacman -S nodejs npm
```
###### angular/cli 6+
```
sudo npm install -g @angular/cli
```

## Specifications
http://andrei.ase.ro/assets/tw/2018/app-specs.pdf

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
