import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { NotifyService } from '../../notify.service';
import { DataService } from '../../../shared/data.service';

export interface DialogData {
  data: string;
}

@Component({
  selector: 'app-temp-dialog',
  templateUrl: './temp-dialog.component.html',
  styleUrls: ['./temp-dialog.component.css']
})
export class TempDialogComponent implements OnInit {

  msg: string = '42';

  constructor(
    public dialogRef: MatDialogRef<TempDialogComponent>,
    private dataService: DataService
  ) {
    setTimeout( () => this.dialogRef.close(), 1000);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  ngOnInit() {
    this.dataService.currentMessage.subscribe(message => this.msg = message);
  }


}
