export interface User {
    uid: string;
    type: string;
    email?: string;
    twitterVerified?: boolean;
    username?: string;
    photoURL?: string;
    coverURL?: string;
    displayName?: string;
    description?: string;
    checkoutId?: string;
    visible?: boolean;
    completedData?: boolean;
    internallyVerified?: boolean;
    accountType?: string;

    address?: any;
    tags?: any;
}