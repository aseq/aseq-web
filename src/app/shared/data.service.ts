import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private messageSource = new BehaviorSubject<string>('default message');
  currentMessage = this.messageSource.asObservable();

  private activeEditorQuiz = new BehaviorSubject<string>('{ "pages": [{ "name": "page1" }]}');
  editorData = this.activeEditorQuiz.asObservable();
  
  entityType = false;


  constructor() { }

  changeMessage(message) {
    this.messageSource.next(message);
  }

  changeActiveEditorQuiz(message) {
    this.activeEditorQuiz.next(message);
  }

  toggleEntity() {
    this.entityType = !this.entityType;
    console.log(this.entityType);
  }
}
