import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { environment } from './../../environments/environment';

@Component({
  selector: 'app-student-pool',
  templateUrl: './student-pool.component.html',
  styleUrls: ['./student-pool.component.css']
})
export class StudentPoolComponent implements OnInit {

  backendHost = environment.backendHost;
  students;

  constructor(
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getStudents();
  }

  getStudents() {
    const studRef = this.http.get(`${this.backendHost}/students`).subscribe((students) => {
      console.log(students);
      this.students = students;
      studRef.unsubscribe();
    });
  }

}
