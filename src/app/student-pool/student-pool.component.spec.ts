import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentPoolComponent } from './student-pool.component';

describe('StudentPoolComponent', () => {
  let component: StudentPoolComponent;
  let fixture: ComponentFixture<StudentPoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentPoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentPoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
