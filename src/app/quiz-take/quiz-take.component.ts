import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { ActivatedRoute } from '@angular/router';

import * as SurveyEditor from 'surveyjs-editor';
import * as Survey from 'survey-angular';

@Component({
  selector: 'app-quiz-take',
  templateUrl: './quiz-take.component.html',
  styleUrls: ['./quiz-take.component.css']
})
export class QuizTakeComponent implements OnInit {

  backendHost = environment.backendHost;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    Survey.StylesManager.applyTheme("bootstrap");

    this.getQuiz();
  }

  getQuiz() {
    this.route.params.subscribe((route) => {
      console.log(route.id);
      this.http.get(`${this.backendHost}/quiz/` + route.id)
      .subscribe((quiz: any) => {
        
        var survey = new Survey.Model(quiz.structure);
        survey.onComplete.add((asd) => {
            console.log(survey.data);
        });

        survey.onValidateQuestion.add(function (s, options) {
          if (options.name == 'animalwater') {
              if(options.value != 'Whale') {
                   options.error = "The answer is not correct. Please give another answer";
               }
           }
       });
        
        Survey.SurveyNG.render('surveyContainer', { model: survey });

      });

    })

  }
}
