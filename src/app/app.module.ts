import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';

import { environment } from '../environments/environment';

// Material Stuff + flex
// import { FlexLayoutModule } from '@angular/flex-layout';
import {MatButtonModule, MatCheckboxModule, MatToolbarModule, MatSidenavModule, MatIconModule, MatListModule, MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material';
import { MatCardModule, MatMenuModule, MatGridListModule, MatInputModule, MatDialogModule } from '@angular/material';
import {MatDialogTitle, MatDialogContent, MatDialogClose, MatFormFieldModule, MatSelectModule, MatChipsModule, MatBadgeModule} from '@angular/material';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle'

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { LoginComponent } from './login/login.component';


import { TempDialogComponent } from './core/notify-comps/temp-dialog/temp-dialog.component';

// FireBase
import { AngularFireModule, FirebaseOptionsToken } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';
export const firebaseConfig = environment.firebaseConfig;



// MyModules
import { CoreModule } from './core/core.module';
import { SignupComponent } from './signup/signup.component';
import { EditorComponent } from './editor/editor.component';
import { SidenavComponent } from './nav/sidenav/sidenav.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { QuizPoolComponent } from './quiz-pool/quiz-pool.component';
import { QuizTakeComponent } from './quiz-take/quiz-take.component';
import { StudentPoolComponent } from './student-pool/student-pool.component';
import { PendingQuizesComponent } from './pending-quizes/pending-quizes.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    LoginComponent,
    SignupComponent,
    EditorComponent,
    SidenavComponent,
    UserSettingsComponent,
    QuizPoolComponent,
    QuizTakeComponent,
    StudentPoolComponent,
    PendingQuizesComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CoreModule,
    ReactiveFormsModule,

    MatButtonModule,
    MatCheckboxModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatMenuModule,
    MatGridListModule,
    MatInputModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatChipsModule,
    MatBadgeModule,
    MatStepperModule,
    MatTooltipModule,
    MatSlideToggleModule,

    AngularFireModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule
  ],
  providers: [
    { provide: FirebaseOptionsToken, useValue: firebaseConfig },
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true} }
  ],
  entryComponents: [
    TempDialogComponent
  ],
  schemas: [
    MatDialogTitle,
    MatDialogContent,
    MatDialogClose
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
